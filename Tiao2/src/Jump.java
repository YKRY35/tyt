import java.io.IOException;
import java.util.Random;

public class Jump {
	final static Random rand = new Random(System.currentTimeMillis());

	public static void main(String[] args) throws IOException, InterruptedException {
		Core c = new Core();
		Runtime r = Runtime.getRuntime();
		Process p;
		int cnt = 0;
		while (true) {
			long time1 = System.currentTimeMillis();

			String path = "screenshot/q" + cnt + ".png";
		//	String savePath="screenshot/q" + cnt + "_.png";

			p = r.exec("cmd /c adb shell /system/bin/screencap -p /sdcard/ss.png");//截图
			p.waitFor();//等待命令执行完毕

			long time2 = System.currentTimeMillis();
			System.out.println("截图时间 " + (time2 - time1));

			p = r.exec("cmd /c adb pull /sdcard/ss.png " + path);//图片保存到本地
			p.waitFor();

			c.calc(path);
			int dis=c.distance;
		//	c.printImg(savePath);//保存到本地分析
			
			System.out.println("press time"+ c.distance);
			
			long time3 = System.currentTimeMillis();
			System.out.println("计算时间 " + (time3 - time2));

			int pressTime = calcPressTime(c.distance);

			String pressPos = getPressPos();
			p = r.exec("cmd /c adb shell input swipe " + pressPos + pressTime);//点击
			p.waitFor();

			long time4 = System.currentTimeMillis();
			System.out.println("触屏时间 " + (time4 - time3));

			cnt++;
			Thread.sleep(1200);//等待人跳完还有白色光圈消失
		}

	}

	static String getPressPos() {
		double sx = 300 + rand.nextInt(100) + rand.nextDouble();
		double sy = 1500 + rand.nextInt(100) + rand.nextDouble();

		double ex = sx + rand.nextInt(30) + rand.nextFloat();
		double ey = sy + rand.nextInt(30) + rand.nextFloat();

		return sx + " " + sy + " " + ex + " " + ey + " ";
	}

	
	//arm 1.32 80
	//x86 1.25 50
	static final double k = 1.23, b = 70;

	static int calcPressTime(int dis) {
		return (int) (k * dis + b);
	}
}