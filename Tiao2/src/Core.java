import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Core {
	final int WIDTH = 1080, HEIGHT = 1920;
	final int HALF_WIDTH = 540, HALF_HEIGHT = 960;

	BufferedImage src;

	final int manRGB = new Color(54, 60, 102).getRGB();// 人臀部颜色
	final Range manSearRange = new Range(200, 1000, 900, 1300);// 人的搜索范围
	Point manPos;// 50ms
	boolean inLeft;

	Range topPosSearRange;
	Point bgBasePos = new Point(540, 650);
	Point topPos;// 目标平台最高点

	final int StageColorDis=5;//平台颜色容差
	final int leapDis = 10;// 颜色搜索最大跨越的距离
	final int sxext = 35;// 搜索横向延伸距离
	Point bottomPos;// 目标平台最低点

	// 木椅子，WZC瓶子,唱片,xx,xx
	final int[] scTopRGB = {
			new Color(214, 179, 146).getRGB(),
			new Color(101, 135, 164).getRGB(),
			new Color(220, 200, 161).getRGB(),
			new Color(136, 101, 99).getRGB(),
			new Color(225, 199, 141).getRGB(),
			new Color(247, 169, 85).getRGB(),
	};
	final Point[] scOffset = {
			new Point(10, 186),
			new Point(25, 50),
			new Point(0, 245),
			new Point(0, 175),
			new Point(0, 175),
			new Point(0, 212),
	};

	Point tarPos;// 目标落点

	int distance;// 距离

	void calc(String path) throws IOException {
		src = ImageIO.read(new File(path));
		
	//	rotate();//旋转图片
		searchMan();
		searchTopPos();
		searchBottomPos();
		calcTarPos();
		calcDis();

	}
	
	void rotate() throws IOException{
		System.out.println(src.getWidth()+" "+src.getHeight());
		BufferedImage o=new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		for(int i=0;i<HEIGHT;i++)
			for(int j=0;j<WIDTH;j++){
				int rgb=src.getRGB(i, j);
				o.setRGB(j,  HEIGHT-1-i, rgb);
			}
		src=o;
	//	ImageIO.write(src, "png", new File("1.png"));
	}

	void searchMan() {
		boolean break_flag = false;
		for (int x = manSearRange.x1; x < manSearRange.x2 && !break_flag; x++)
			for (int y = manSearRange.y1; y < manSearRange.y2; y++)
				if (src.getRGB(x, y) == manRGB) {
					manPos = new Point(x, y);
					break_flag = true;
					break;
				}
		if (manPos.x < HALF_WIDTH)
			inLeft = true;
		else
			inLeft = false;
	}

	void searchTopPos() {
		if (inLeft) {// 搜索右侧
			topPosSearRange = new Range(HALF_WIDTH, bgBasePos.y, WIDTH, HEIGHT);
		} else {// 搜索左侧
			topPosSearRange = new Range(0, bgBasePos.y, HALF_WIDTH, HEIGHT);
		}
		final int bgRGB = src.getRGB(bgBasePos.x, bgBasePos.y);

		for (int y = topPosSearRange.y1; y < topPosSearRange.y2; y++) {
			for (int x = topPosSearRange.x1; x < topPosSearRange.x2; x++) {
				if (!ColorUtil.similar(src.getRGB(x, y), bgRGB, 30)) {// 不是背景色
					topPos = new Point(x, y);
					// markPoint(x,y);
					return;
				}
			}
		}
	}

	void searchBottomPos() {
		if (specialCheck())
			return;// 特判

		int stageRGB = src.getRGB(topPos.x, topPos.y);
		Point bpl = topPos, bpr = topPos;

		for (int y = topPos.y; y < bpl.y + leapDis && y < HEIGHT; y++) {
			int xl = bpl.x - sxext, xr = bpr.x + sxext;
			if (xl < 0)
				xl = 0;
			if (xr > WIDTH)
				xr = WIDTH;
			for (int x = xl; x < xr; x++) {
				if (ColorUtil.similar(src.getRGB(x, y), stageRGB, StageColorDis)) {
					bpl = new Point(x, y);
					break;
				}
			}
			for (int x = xr - 1; x > xl; x--) {
				if (ColorUtil.similar(src.getRGB(x, y), stageRGB, StageColorDis)) {
					bpr = new Point(x, y);
					break;
				}
			}
		}
		bottomPos = new Point((bpl.x + bpr.x) / 2, bpl.y);
		
	//	 markPoint(topPos.x,topPos.y);
	//	 markPoint(bottomPos.x,bottomPos.y);
	}

	boolean specialCheck() {//特判
		for (int i = scTopRGB.length - 1; i >= 0; i--) {
			for(int y=topPos.y;y<HEIGHT;y++){
				if (src.getRGB(topPos.x, y) == scTopRGB[i]) {
					bottomPos = new Point(topPos.x + scOffset[i].x, topPos.y + scOffset[i].y);
					return true;
				}
			}
		}

		return false;
	}

	void calcTarPos() {
		tarPos = new Point((topPos.x + bottomPos.x) / 2, (topPos.y + bottomPos.y) / 2);

	//	markPoint(tarPos.x, tarPos.y);
	}

	void calcDis() {
		distance = manPos.distance(tarPos);
	}

	final int markSize = 3;

	void markPoint(int x, int y) {//标记某个点，然后保存成图片，便于分析
		for (int i = x - markSize; i <= x + markSize; i++) {
			for (int j = y - markSize; j <= y + markSize; j++) {
				src.setRGB(i, j, 1000);
			}
		}
	}

	void printImg(String fn) throws IOException {
		ImageIO.write(src, "png", new File(fn));
	}
}

class ColorUtil {
	static boolean similar(int s1, int s2, int SD) {//颜色相似 颜色1 颜色2 距离
		Color c1 = new Color(s1), c2 = new Color(s2);
		int r1 = c1.getRed(), g1 = c1.getGreen(), b1 = c1.getBlue();
		int r2 = c2.getRed(), g2 = c2.getGreen(), b2 = c2.getBlue();
		int dis = (int) Math.sqrt(Math.pow(r1 - r2, 2) + Math.pow(g1 - g2, 2) + Math.pow(b1 - b2, 2));

		// System.out.println("dis "+dis);
		return dis < SD;
	}
}

class Point {
	int x, y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public String toString() {
		return this.x + " " + this.y;
	}

	int distance(Point p) {
		return (int) Math.sqrt(Math.pow(x - p.x, 2) + Math.pow(y - p.y, 2));
	}
}

class Range {
	int x1, y1, x2, y2;

	public Range(int x1, int y1, int x2, int y2) {//左上角 右下角
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
}