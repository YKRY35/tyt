import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Else01 {
	public static void main(String[] args) throws IOException {
		final int manRGB = new Color(54, 60, 102).getRGB();// 人臀部颜色
		final int replaceRGB = new Color(255, 0, 0).getRGB();
		int cnt = 0;

		BufferedImage src = ImageIO.read(new File("ELSE01/s0.png"));

		test(src);
	//	solve(src);
	}
	
	static void test(BufferedImage img) throws IOException{
		final int similarDis_test=45;
		int w = img.getWidth();
		int h = img.getHeight();
		int px = -1, py = -1;
		for (int i = 0; i < w; i += 1)
			for (int j = h - 1; j >= 0; j -= 1)
				if (ColorUtil.similar(img.getRGB(i, j), manRGB, similarDis_test)) {
					img.setRGB(i, j, Color.RED.getRGB());
				}
		ImageIO.write(img, "png", new File("ELSE01/out.png"));
	}

	static final int manRGB = new Color(54, 60, 102).getRGB();// 人臀部颜色
	static final int similarDis = 60;
	static final int xSpan = 60, ySpan = 35;
	static final float yRatio = 0.90f;

	static void solve(BufferedImage img) {
		int w = img.getWidth();
		int h = img.getHeight();
		int px = -1, py = -1;
		for (int i = 0; i < w; i += xSpan)
			for (int j = h - 1; j >= 0; j -= ySpan)
				if (ColorUtil.similar(img.getRGB(i, j), manRGB, similarDis)) {
					px = i;
					py = j;
					break;
				}
		int[] xExtend = new int[2];
		for (int k = -1; k <= 1; k += 2) {
			int index = k < 0 ? 0 : 1;
			for (int i = px; i >= 0 && i < w; i += k) {
				if (!ColorUtil.similar(img.getRGB(i, py), manRGB, similarDis))
					break;
				xExtend[index] = i;
			}
		}
		int xMiddle = (xExtend[0] + xExtend[1]) / 2;

		int[] yExtend = new int[2];// 0 top 1 bottom
		for (int k = -1; k <= 1; k += 2) {
			int index = k < 0 ? 0 : 1;
			for (int j = py; j >= 0 && j < w; j += k) {
				if (!ColorUtil.similar(img.getRGB(xMiddle, j), manRGB, similarDis))
					break;
				yExtend[index] = j;
			}
		}
		int yTar = (int) ((yExtend[1] - yExtend[0]) * yRatio + yExtend[0]);
		System.out.println(xMiddle + " " + yTar);
	}
}