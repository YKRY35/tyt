package com.zhou.tythelper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class HelperView extends View {

    public HelperView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public HelperView(Context context) {
        super(context);
    }

    boolean inited = false;
    int width, height;
    Paint paint;

    int helperTTL;
    final int MAX_helperTTL = 2 * 50;

    Point manPos;
    Point lineTarPos;
    long pressTime;
    final int xSpeed = 710, ySpeed = 360;
    int xDir, yDir = -1;

    void updateHelper(Point p) {
        this.helperTTL = MAX_helperTTL;
        manPos = p;
        lineTarPos = new Point(manPos.x, manPos.y);
        if (manPos.x < width / 2) xDir = 1;
        else xDir = -1;
        pressTime = System.currentTimeMillis();
    }

    void init() {
        width = getWidth();
        height = getHeight();
        Log.i("View", width + " " + height);
        paint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!inited) {
            init();
            inited = true;
        }
        super.onDraw(canvas);
        drawButton(canvas);
        drawHelper(canvas);
        postInvalidate();
    }

    void drawButton(Canvas canvas) {
        if (prepared) {
            paint.setARGB(100, 255, 0, 0);
        } else {
            paint.setARGB(100, 0, 255, 0);
        }
        canvas.drawRect(0, 0, BUTTON_X, height, paint);
    }

    void drawHelper(Canvas canvas) {
        if (helperTTL > 0) {
            paint.setColor(Color.RED);
            paint.setStrokeWidth(6.0f);
            canvas.drawLine(manPos.x, manPos.y, lineTarPos.x, lineTarPos.y, paint);
            lineTarPos.x = (int) (manPos.x + xDir * (System.currentTimeMillis() - pressTime) * xSpeed / 1000);
            lineTarPos.y = (int) (manPos.y + yDir * (System.currentTimeMillis() - pressTime) * ySpeed / 1000);

            helperTTL--;
        }
    }

    boolean prepared = false;

    final int BUTTON_X = 150;//注意同步

    public void updateButton(boolean prepared) {
        this.prepared = prepared;
        postInvalidate();
    }
}
