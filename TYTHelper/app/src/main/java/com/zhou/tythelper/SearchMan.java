package com.zhou.tythelper;


import android.graphics.Bitmap;
import android.graphics.Color;

public class SearchMan {
    //算法是
    //第一次跨度较大地寻找
    //找到以后在这个范围内继续搜索

    static final int manRGB = Color.rgb(54, 60, 102);// 人臀部颜色
    static final int similarDis = 41;
    static final int xSpan = 60, ySpan = 35;
    static final float yRatio = 0.90f;

    public static Point start(Bitmap img) {
        int w = img.getWidth();
        int h = img.getHeight();
        int px = -1, py = -1;
        for (int i = 0; i < w; i += xSpan)
            for (int j = h - 1; j >= 0; j -= ySpan)
                if (ColorUtil.similar(img.getPixel(i, j), manRGB, similarDis)) {
                    px = i;
                    py = j;
                    break;
                }
        int[] xExtend = new int[2];
        for (int k = -1; k <= 1; k += 2) {
            int index = k < 0 ? 0 : 1;
            for (int i = px; i >= 0 && i < w; i += k) {
                if (!ColorUtil.similar(img.getPixel(i, py), manRGB, similarDis))
                    break;
                xExtend[index] = i;
            }
        }
        int xMiddle = (xExtend[0] + xExtend[1]) / 2;

        int[] yExtend = new int[2];// 0 top 1 bottom
        for (int k = -1; k <= 1; k += 2) {
            int index = k < 0 ? 0 : 1;
            for (int j = py; j >= 0 && j < h; j += k) {
                if (!ColorUtil.similar(img.getPixel(xMiddle, j), manRGB, similarDis))
                    break;
                yExtend[index] = j;
            }
        }
        int yTar = (int) ((yExtend[1] - yExtend[0]) * yRatio + yExtend[0]);

        return new Point(xMiddle,yTar);
    }
}

class Point {
    int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    @Override
    public String toString(){
        return "pos   "+x+" "+y;
    }
}

class ColorUtil {
    static boolean similar(int s1, int s2, int SD) {//颜色相似 颜色1 颜色2 距离
        int r1 = Color.red(s1), g1 = Color.green(s1), b1 = Color.blue(s1);
        int r2 = Color.red(s2), g2 = Color.green(s2), b2 = Color.blue(s2);
        int dis = (int) Math.sqrt(Math.pow(r1 - r2, 2) + Math.pow(g1 - g2, 2) + Math.pow(b1 - b2, 2));
        return dis < SD;
    }
}