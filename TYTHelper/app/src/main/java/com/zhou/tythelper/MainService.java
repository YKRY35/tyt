package com.zhou.tythelper;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Environment;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;


public class MainService extends Service implements View.OnTouchListener, ImageReader.OnImageAvailableListener {
    final String TAG = "MainService";

    int sw, sh;

    WindowManager windowManager;
    WindowManager.LayoutParams params;

    HelperView hv;
    int statusBarHeight = -1;
    MediaProjection mediaProjection;

    ImageReader screenReader;

    MediaProjectionManager projectionManager;
    int resultCode;
    Intent captureIntent;
    VirtualDisplay virtualDisplay;

    boolean receiveFirstImage=false;

    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    void init() {
        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        DisplayMetrics dm = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(dm);
        sw = dm.widthPixels;
        sh = dm.heightPixels;

        params = new WindowManager.LayoutParams();
        params.x = params.y = 0;
        params.width = sw;
        params.height = 500;
        params.gravity = Gravity.LEFT | Gravity.TOP;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;
        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        params.format = PixelFormat.RGBA_8888;

        hv = new HelperView(this);

        hv.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        hv.setOnTouchListener(this);

        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        windowManager.addView(hv, params);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initCapture(intent);
        return super.onStartCommand(intent, flags, startId);
    }


    void initCapture(Intent intent) {
        this.resultCode = intent.getIntExtra("resultCode", -1);
        this.captureIntent = intent;
        projectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        screenReader = ImageReader.newInstance(sw, sh, PixelFormat.RGBA_8888, 1);
        screenReader.setOnImageAvailableListener(this,null);
        mediaProjection = projectionManager.getMediaProjection(resultCode, captureIntent);
        virtualDisplay=mediaProjection.createVirtualDisplay("screenReader", sw, sh, DisplayMetrics.DENSITY_DEVICE_STABLE,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_PRESENTATION, null, null, null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        windowManager.removeView(hv);
        mediaProjection.stop();
        screenReader.setOnImageAvailableListener(null, null);
    }

    @Override
    public void onImageAvailable(ImageReader reader) {
        virtualDisplay.setSurface(null);

        Image image = reader.acquireLatestImage();

        if(receiveFirstImage){
            if(image!=null) image.close();
            return;
        }

        receiveFirstImage=true;

        if (image != null) {
            Image.Plane[] planes = image.getPlanes();
            if (planes.length > 0) {
                final Image.Plane plane = planes[0];
                final ByteBuffer buffer = plane.getBuffer();
                final int rowStride = plane.getRowStride();
                final int pixelStride = plane.getPixelStride();
                final int rowPadding=rowStride-pixelStride*image.getWidth();

                buffer.position(rowStride*(params.y+statusBarHeight));
                Bitmap bitmap = Bitmap.createBitmap(image.getWidth()+rowPadding/pixelStride, params.height, Bitmap.Config.ARGB_8888);
                bitmap.copyPixelsFromBuffer(buffer);

                captureFinished(bitmap);

                if (bitmap != null && !bitmap.isRecycled()) bitmap.recycle();

            //    Log.i(TAG,"used time   "+(System.currentTimeMillis()-st));

            }
        }

        if (image != null)
            image.close();

    }

    long st;

    void capture() {//截图
    //    st=System.currentTimeMillis();
        receiveFirstImage=false;
        virtualDisplay.setSurface(screenReader.getSurface());
    }

    int cnt = 0;

    void test_saveBitmap(Bitmap bitmap) {
        File sdcardD = Environment.getExternalStorageDirectory();
        File imageD = new File(sdcardD, "com.Zhou.TYTHelper");
        if (!imageD.exists()) imageD.mkdirs();
        File imageF = new File(imageD, cnt++ + ".png");
        if (!imageF.exists()) try {
            imageF.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            OutputStream os = new FileOutputStream(imageF);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean prepared = false;
    final int BUTTON_X = 150;//注意同步

    boolean movingFlag = false;
    float lstX, lstY;
    float offsetX, offsetY;

    //getX 是相对View的
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float mx = event.getRawX();
        float my = event.getRawY() - statusBarHeight;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN://内部  移动
                float _x = event.getX(), _y = event.getY();
                if (_x > 0 && _x < 150 && _y > 0 && _y < params.height) {//控制按钮
                    movingFlag = false;
                    prepared = !prepared;
                    hv.updateButton(prepared);
                } else {
                    movingFlag = true;
                    lstX = mx;
                    lstY = my;
                    formatParams();
                    offsetX = lstX - params.x;
                    offsetY = lstY - params.y;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (movingFlag) {
                    params.x = (int) (mx - offsetX);
                    params.y = (int) (my - offsetY);
                    windowManager.updateViewLayout(hv, params);
                }
                break;
            case MotionEvent.ACTION_UP:
                formatParams();
                break;
            case MotionEvent.ACTION_OUTSIDE://外部
                if (prepared) {
                    testTime1=System.currentTimeMillis();
                    Log.i(TAG, "capture!!");
                    capture();
                }
                break;
        }
        return true;
    }

    void formatParams() {
        if (params.x < 0) params.x = 0;
        if (params.x > sw) params.x = sw;
        if (params.y < 0) params.y = 0;
        if (params.y > sh) params.y = sh;
    }


    final Rect manSearRange = new Rect(200, 1000, 900, 1300);// 人的搜索范围
    final int manButtocksRGB = Color.rgb(54, 60, 102);

    boolean purpleManFounded = false;
    int purpleManX = 0, purpleManY = 0;

    long testTime1=0;
    public void captureFinished(Bitmap bitmap) {
        //test_saveBitmap(bitmap);

        final Bitmap nBitmap = Bitmap.createBitmap(bitmap, 0, 0, sw, params.height);//在新线程处理前拷贝一份出来
        new Thread() {
            public void run() {//用户操作上保证同步
                searchPurpleMan(nBitmap);
                if (!nBitmap.isRecycled()) nBitmap.recycle();
            }
        }.start();
    }

    void searchPurpleMan(Bitmap bitmap) {
        Log.i(TAG,"testTime1   "+(System.currentTimeMillis()-testTime1));

        Point manPos=SearchMan.start(bitmap);
        hv.updateHelper(manPos);
        Log.i("man pos",manPos+"");

        purpleManFounded = true;
        Log.i(TAG,"testTime2   "+(System.currentTimeMillis()-testTime1));
    }

}