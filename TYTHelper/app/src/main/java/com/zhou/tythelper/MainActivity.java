package com.zhou.tythelper;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.hardware.display.DisplayManager;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Syn syn=new Syn();//初始化标志

    ToggleButton toggle;

    Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    int sw,sh;

    void init(){
        WindowManager windowManager=(WindowManager)getSystemService(Context.WINDOW_SERVICE);

        DisplayMetrics dm=new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(dm);
        sw=dm.widthPixels;
        sh=dm.heightPixels;

        getPermission();//获取权限

        toggle= (ToggleButton) findViewById(R.id.toggle);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {//设置按钮事件
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                synchronized (syn){
                    if(!syn.inited){
                        return ;
                    }
                }
                if(isChecked){
                    _startService();
                }else{
                    _stopService();
                }
            }
        });
    }

    void getPermission(){
        //write
        if(Build.VERSION.SDK_INT>=23){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},0);
            }
        }

        //screen shot
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            MediaProjectionManager projectionManager= (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
            Intent intent=projectionManager.createScreenCaptureIntent();
            startActivityForResult(intent,0);
        }else{
            Toast.makeText(this, "Android 版本过低", Toast.LENGTH_SHORT).show();
            finish();
        }

        //overlay
        if(Build.VERSION.SDK_INT>=23){//需要动态获得
            if (!Settings.canDrawOverlays(this)) {//没有权限
                Intent intent=new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                startActivity(intent);
                finish();
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        serviceIntent=new Intent(this,MainService.class);
        serviceIntent.putExtras(data);
        serviceIntent.putExtra("resultCode",resultCode);

        synchronized (syn){
            syn.inited=true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        _stopService();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void _startService(){
        Toast.makeText(this,"服务启动",Toast.LENGTH_SHORT).show();
        startService(serviceIntent);
    }
    void _stopService(){
        Toast.makeText(this,"服务停止",Toast.LENGTH_SHORT).show();
        stopService(serviceIntent);
    }
}

class Syn{
    boolean inited;
    public Syn(){
        inited=false;
    }
}
